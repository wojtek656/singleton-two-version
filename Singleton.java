public class Singleton {

    private static Singleton instance = null;

    private Singleton() {
        System.out.print("Singleton created!");
    }

    public static synchronized Singleton getInstance()
    {
        if(instance == null)
            instance = new Singleton();

        return instance;
    }



}
